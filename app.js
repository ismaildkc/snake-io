var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var express = require('express');
var path = require('path');

app.use(express.static(__dirname + '/public'));



app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/admin', function(req, res){
  res.sendFile(__dirname + '/admin.html');
});
app.get('/test', function(req, res){
  res.sendFile(__dirname + '/test.html');
});

io.on('connection', function(socket){
	socket.on('taraftar tribune', function(msg){
	    io.emit('taraftar tribune', msg);
	    console.log(msg);
  	});

  socket.on('disconnect', function(){
    
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});