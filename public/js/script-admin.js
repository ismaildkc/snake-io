$(".left").click(function() {
  jQuery.event.trigger({ type: 'keydown', which: 37 });
  socket.emit('taraftar tribune', 'left');  
});
$(".up").click(function() {
  jQuery.event.trigger({ type: 'keydown', which: 38 });
  socket.emit('taraftar tribune', 'up');  
});
$(".right").click(function() {
  jQuery.event.trigger({ type: 'keydown', which: 39 });
  socket.emit('taraftar tribune', 'right');  
});
$(".down").click(function() {
  jQuery.event.trigger({ type: 'keydown', which: 40 });
  socket.emit('taraftar tribune', 'down');  
});
