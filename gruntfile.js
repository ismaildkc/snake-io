module.exports = function(grunt){
	grunt.initConfig({
		pkg:grunt.file.readJSON('package.json'),

		watch: {
			sass: {
				files: ['public/scss/*.scss'],
				tasks: ['sass'],
			}
		},

		sass: {
			dist: {
				files: {
					'public/css/style.css' : 'public/scss/style.scss'
				}
			}
		},

		
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.registerTask('default', ["watch"]);
};